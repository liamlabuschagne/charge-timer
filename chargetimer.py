#!/usr/bin/python3
import time
import notify2
chargerIn = True
notify2.init('Charge Timer')
sleepTime = 60 * 20 # Every 20 minutes (60 seconds x 20 = 20 minutes) 


def trusty_sleep(n):
    start = time.time()
    while (time.time() - start < n):
        time.sleep(n - (time.time() - start))

while True:
	trusty_sleep(sleepTime)
	if(chargerIn):
		n = notify2.Notification('Remove your charger', 'For best battery health, remove your charger to give it a rest.','/home/liam/Dev/Python/battery.png')
		n.show()
		chargerIn = False
	else:
		n = notify2.Notification('Insert your charger', 'For best battery health, insert your charger to give it some juice..','/home/liam/Dev/Python/battery.png')
		n.show()
		chargerIn = True